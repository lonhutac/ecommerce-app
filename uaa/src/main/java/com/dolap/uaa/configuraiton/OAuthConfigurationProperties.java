package com.dolap.uaa.configuraiton;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "uaa.oauth")
public class OAuthConfigurationProperties {

    private String clientId;
    private String clientSecret;
    private Integer tokenValiditySeconds;
}
