package com.dolap.uaa.configuraiton;

import com.dolap.uaa.domain.repository.UserRepository;
import com.dolap.uaa.mapper.UserMapper;
import com.dolap.uaa.service.CustomUserDetailsService;
import com.dolap.uaa.service.SessionService;
import com.dolap.uaa.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
public class ServiceConfiguration {

    @Bean
    public UserService userService(UserRepository userRepository, UserMapper userMapper) {
        return new UserService(userRepository, userMapper);
    }

    @Bean
    public SessionService sessionService(UserService userService) {
        return new SessionService(userService);
    }

    @Bean
    public UserDetailsService userDetailsService(UserService userService) {
        return new CustomUserDetailsService(userService);
    }
}
