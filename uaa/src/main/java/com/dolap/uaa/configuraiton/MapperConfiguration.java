package com.dolap.uaa.configuraiton;

import com.dolap.uaa.mapper.UserMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    @Bean
    public UserMapper userMapper() {
        return new UserMapper();
    }
}
