package com.dolap.uaa.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@AllArgsConstructor
@Document(collection = "users")
public class User implements UserDetails {

    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String surname;
    @Field
    private String username;
    @Field
    private String email;
    @Field
    private String password;
    @Field
    private boolean enabled = true;
    @Field
    private boolean accountNonExpired = true;
    @Field
    private boolean accountNonLocked = true;
    @Field
    private boolean credentialsNonExpired = true;
    @Field
    private Set<String> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream()
                .map(role -> (GrantedAuthority) () -> role)
                .collect(Collectors.toList());
    }

    public boolean hasRole(String role) {
        return roles.contains(role);
    }
}
