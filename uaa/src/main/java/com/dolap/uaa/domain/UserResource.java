package com.dolap.uaa.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResource {

    private String id;
    private String name;
    private String surname;
    private String username;
    private String email;
    private Boolean enabled;
    private Set<String> roles;
}
