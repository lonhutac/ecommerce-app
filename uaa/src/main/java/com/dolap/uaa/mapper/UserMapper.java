package com.dolap.uaa.mapper;

import com.dolap.uaa.domain.CreateUserRequest;
import com.dolap.uaa.domain.UserResource;
import com.dolap.uaa.domain.entity.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {

    public UserResource entity2Resource(User entity) {
        return UserResource.builder()
                .id(entity.getId())
                .name(entity.getName())
                .surname(entity.getSurname())
                .username(entity.getUsername())
                .email(entity.getEmail())
                .enabled(entity.isEnabled())
                .roles(entity.getRoles())
                .build();
    }

    public List<UserResource> entity2Resource(List<User> entities) {
        return entities
                .stream()
                .map(this::entity2Resource)
                .collect(Collectors.toList());
    }

    public User createRequest2Entity(CreateUserRequest request) {
        return User.builder()
                .name(request.getName())
                .surname(request.getSurname())
                .username(request.getUsername())
                .email(request.getEmail())
                .password(request.getPassword())
                .roles(request.getRoles())
                .enabled(true)
                .accountNonLocked(true)
                .accountNonExpired(true)
                .credentialsNonExpired(true)
                .build();
    }
}
