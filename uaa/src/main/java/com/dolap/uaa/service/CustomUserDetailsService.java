package com.dolap.uaa.service;

import com.dolap.uaa.domain.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional;
        if (ObjectUtils.isEmpty(username)) {
            throw new AuthenticationCredentialsNotFoundException("Username must be provided!");
        }

        if (username.contains("@")) {
            userOptional = userService.findByEmailIgnoreCase(username);
        } else {
            userOptional = userService.findByUsernameIgnoreCase(username);
        }

        if (userOptional.isPresent()) {
            if (!userOptional.get().isEnabled()) {
                throw new DisabledException("User is not enabled, please contact with admin");
            }
            return userOptional.get();
        }
        throw new UsernameNotFoundException("Bad Credentials!");
    }
}
