package com.dolap.uaa.service;

import com.dolap.uaa.domain.CreateUserRequest;
import com.dolap.uaa.domain.UserResource;
import com.dolap.uaa.domain.entity.User;
import com.dolap.uaa.domain.repository.UserRepository;
import com.dolap.uaa.exception.ConflictException;
import com.dolap.uaa.exception.RecordNotFoundException;
import com.dolap.uaa.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public List<UserResource> findAll() {
        final List<User> users = userRepository.findAll();
        return userMapper.entity2Resource(users);
    }

    public Optional<User> findByUsernameIgnoreCase(String username) {
        return userRepository.findByUsernameIgnoreCase(username);
    }

    public Optional<User> findByEmailIgnoreCase(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

    public UserResource getUser(String id) {
        final User user = getUserById(id);
        return userMapper.entity2Resource(user);
    }

    public UserResource createUser(CreateUserRequest request) {
        validateCreateRequest(request);
        final User user = userMapper.createRequest2Entity(request);
        final User saved = userRepository.save(user);
        return userMapper.entity2Resource(saved);
    }

    private void validateCreateRequest(CreateUserRequest request) {
        final Optional<User> userByUsernameOptional = findByUsernameIgnoreCase(request.getUsername());
        if (userByUsernameOptional.isPresent()) {
            log.debug("User not created | Username must be unique: {}", request.getUsername());
            throw new ConflictException("Username must be unique!");
        }

        final Optional<User> userByEmailOptional = findByEmailIgnoreCase(request.getEmail());
        if (userByEmailOptional.isPresent()) {
            log.debug("User not created | Email must be unique!");
            throw new ConflictException("Email must be unique!");
        }
    }

    private User getUserById(String id) {
        final Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) {
            log.error("User could not be found because of it is not exist!");
            throw new RecordNotFoundException("User not found!");
        }
        return userOptional.get();
    }
}
