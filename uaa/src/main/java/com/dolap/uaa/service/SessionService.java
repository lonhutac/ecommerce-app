package com.dolap.uaa.service;

import com.dolap.uaa.domain.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

@RequiredArgsConstructor
public class SessionService {

    private final UserService userService;

    public UserDetails getPrincipal(UserDetails user) {
        Optional<User> userOptional = userService.findByUsernameIgnoreCase(user.getUsername());
        return userOptional.orElseThrow(() -> new AuthenticationCredentialsNotFoundException("Authentication failed!"));
    }
}
