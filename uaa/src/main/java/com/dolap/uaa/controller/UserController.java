package com.dolap.uaa.controller;

import com.dolap.uaa.domain.CreateUserRequest;
import com.dolap.uaa.domain.UserResource;
import com.dolap.uaa.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/api/uaa/users")
    public List<UserResource> getAll() {
        return userService.findAll();
    }

    @GetMapping("/api/uaa/users/{id}")
    public UserResource getUser(@PathVariable String id) {
        return userService.getUser(id);
    }

    @PostMapping("/api/uaa/users/register")
    @ResponseStatus(HttpStatus.CREATED)
    public UserResource createUser(@RequestBody CreateUserRequest request) {
        return userService.createUser(request);
    }
}
