package com.dolap.uaa.controller;

import com.dolap.uaa.service.SessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SessionController {

    private final SessionService sessionService;

    @GetMapping("/api/uaa/sessions")
    public UserDetails user(@AuthenticationPrincipal UserDetails user) {
        return sessionService.getPrincipal(user);
    }
}
