package com.dolap.categoryApp.mapper;

import com.dolap.categoryApp.api.model.CategoryResponse;
import com.dolap.categoryApp.domain.entity.Category;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(MockitoJUnitRunner.class)
public class CategoryMapperTest {

    private CategoryMapper categoryMapper;

    @Before
    public void setup() {
        categoryMapper = new CategoryMapper();
    }

    /**
     * {@link CategoryMapper#convertToResponse(List)}
     */
    @Test
    public void should_map_categories_to_response() {
        // given
        List<Category> categories = buildCategories();
        List<CategoryResponse> expectedCategoryResponses = buildCategoryResponses();

        // then
        List<CategoryResponse> categoryResponses = categoryMapper.convertToResponse(categories);

        assertThat("Response can not have null value", categoryResponses, Matchers.notNullValue());
        assertThat("Missing some categories in mapping", categoryResponses, hasSize(3));

        assertThat(expectedCategoryResponses.get(0).getId(), Matchers.equalTo(categoryResponses.get(0).getId()));
        assertThat(expectedCategoryResponses.get(0).getName(), Matchers.equalTo(categoryResponses.get(0).getName()));
        assertThat(expectedCategoryResponses.get(0).getParentId(), Matchers.equalTo(categoryResponses.get(0).getParentId()));

        assertThat(expectedCategoryResponses.get(1).getId(), Matchers.equalTo(categoryResponses.get(1).getId()));
        assertThat(expectedCategoryResponses.get(1).getName(), Matchers.equalTo(categoryResponses.get(1).getName()));
        assertThat(expectedCategoryResponses.get(1).getParentId(), Matchers.equalTo(categoryResponses.get(1).getParentId()));

        assertThat(expectedCategoryResponses.get(2).getId(), Matchers.equalTo(categoryResponses.get(2).getId()));
        assertThat(expectedCategoryResponses.get(2).getName(), Matchers.equalTo(categoryResponses.get(2).getName()));
        assertThat(categoryResponses.get(2).getParentId(), Matchers.equalTo(null));
    }

    /**
     * {@link CategoryMapper#convertToResponse(List)}
     */
    @Test
    public void should_map_empty_categories_to_empty_response() {
        // then
        List<CategoryResponse> categoryResponses = categoryMapper.convertToResponse(Collections.emptyList());

        assertThat("Response can not have null value", categoryResponses, Matchers.notNullValue());
        assertThat("Response should be empty", categoryResponses, hasSize(0));
    }

    private List<CategoryResponse> buildCategoryResponses() {
        return Arrays.asList(
                CategoryResponse.builder()
                        .id("categoryId1")
                        .name("categoryName1")
                        .parentId("parentId1")
                        .build(),
                CategoryResponse.builder()
                        .id("categoryId2")
                        .name("categoryName2")
                        .parentId("parentId2")
                        .build(),
                CategoryResponse.builder()
                        .id("categoryId3")
                        .name("categoryName3")
                        .build()
        );
    }

    private List<Category> buildCategories() {
        return Arrays.asList(
                Category.builder()
                        .id("categoryId1")
                        .name("categoryName1")
                        .parentId("parentId1")
                        .build(),
                Category.builder()
                        .id("categoryId2")
                        .name("categoryName2")
                        .parentId("parentId2")
                        .build(),
                Category.builder()
                        .id("categoryId3")
                        .name("categoryName3")
                        .build()
        );
    }
}
