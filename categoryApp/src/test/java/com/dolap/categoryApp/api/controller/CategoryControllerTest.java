package com.dolap.categoryApp.api.controller;

import com.dolap.categoryApp.api.model.CategoryResponse;
import com.dolap.categoryApp.service.CategoryService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CategoryController.class)
public class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryService;

    /**
     * {@link CategoryController#getCategories()}
     */
    @Test
    @SneakyThrows
    public void should_get_categories() {

        // given
        final List<CategoryResponse> categories = buildTestCategories();

        // when
        Mockito.when(categoryService.getCategories()).thenReturn(categories);

        final ResultActions resultActions = mockMvc
                .perform(get("/api/v1/categories"))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id").exists())
                .andExpect(jsonPath("$.[*].name").exists())
                .andExpect(jsonPath("$.[*].parentId").doesNotExist());
    }

    @Test
    @SneakyThrows
    public void should_get_category() {
        // given
        final CategoryResponse category = buildTestCategory();

        // when
        Mockito.when(categoryService.getCategoryById(anyString())).thenReturn(category);

        final ResultActions resultActions = mockMvc
                .perform(get("/api/v1/categories/testId"))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.id", is("categoryId1")))
                .andExpect(jsonPath("$.name", is("categoryName1")));
    }

    private CategoryResponse buildTestCategory() {
        return CategoryResponse.builder()
                .id("categoryId1")
                .name("categoryName1")
                .build();
    }

    private List<CategoryResponse> buildTestCategories() {
        return Arrays.asList(
                CategoryResponse.builder()
                        .id("categoryId1")
                        .name("categoryName1")
                        .build(),
                CategoryResponse.builder()
                        .id("categoryId2")
                        .name("categoryName2")
                        .build(),
                CategoryResponse.builder()
                        .id("categoryId2")
                        .name("categoryName2")
                        .build()
        );
    }
}
