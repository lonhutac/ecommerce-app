package com.dolap.categoryApp.service;

import com.dolap.categoryApp.api.model.CategoryResponse;
import com.dolap.categoryApp.domain.entity.Category;
import com.dolap.categoryApp.domain.exception.RecordNotFoundException;
import com.dolap.categoryApp.domain.repository.CategoryRepository;
import com.dolap.categoryApp.mapper.CategoryMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {

    private CategoryService categoryService;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private CategoryMapper categoryMapper;

    @Before
    public void setUp() {
        categoryService = new CategoryService(categoryRepository, categoryMapper);
    }

    /**
     * {@link CategoryService#getCategories()}
     */
    @Test
    public void should_get_categories() {
        // given
        final List<CategoryResponse> categories = buildCategories();

        // when
        when(categoryRepository.findAllByParentIdIsNull()).thenReturn(Collections.emptyList());
        when(categoryMapper.convertToResponse(anyList())).thenReturn(categories);

        // then
        final List<CategoryResponse> categoriesResponse = categoryService.getCategories();

        assertThat(categoriesResponse.get(0).getId(), Matchers.equalTo(categories.get(0).getId()));
        assertThat(categoriesResponse.get(0).getName(), Matchers.equalTo(categories.get(0).getName()));
        assertThat(categoriesResponse.get(1).getId(), Matchers.equalTo(categories.get(1).getId()));
        assertThat(categoriesResponse.get(1).getName(), Matchers.equalTo(categories.get(1).getName()));
        assertThat(categoriesResponse.get(2).getId(), Matchers.equalTo(categories.get(2).getId()));
        assertThat(categoriesResponse.get(2).getName(), Matchers.equalTo(categories.get(2).getName()));
    }

    /**
     * {@link CategoryService#getCategoryById(String)}
     */
    @Test
    public void should_get_category_detail() {
        // given
        final CategoryResponse expectedCategoryResponse = buildCategory();

        // when
        when(categoryRepository.findById(anyString())).thenReturn(Optional.of(new Category()));
        when(categoryMapper.convertToResponse(any(Category.class))).thenReturn(expectedCategoryResponse);

        // then
        final CategoryResponse categoryResponse = categoryService.getCategoryById("testId");

        assertThat(expectedCategoryResponse.getId(), Matchers.equalTo(categoryResponse.getId()));
        assertThat(expectedCategoryResponse.getName(), Matchers.equalTo(categoryResponse.getName()));
    }

    /**
     * {@link CategoryService#getCategoryById(String)}
     */
    @Test(expected = RecordNotFoundException.class)
    public void should_throw_exception_when_get_category_detail_with_invalid_id() {
        // when
        when(categoryRepository.findById(anyString())).thenThrow(RecordNotFoundException.class);

        // then
        categoryService.getCategoryById("testId");

        fail("Code should not reach here");
    }

    private CategoryResponse buildCategory() {
        return CategoryResponse.builder()
                .id("categoryId1")
                .name("categoryName1")
                .build();
    }

    private List<CategoryResponse> buildCategories() {
        return Arrays.asList(
                CategoryResponse.builder()
                        .id("categoryId1")
                        .name("categoryName1")
                        .build(),
                CategoryResponse.builder()
                        .id("categoryId2")
                        .name("categoryName2")
                        .build(),
                CategoryResponse.builder()
                        .id("categoryId3")
                        .name("categoryName3")
                        .build()
        );
    }
}
