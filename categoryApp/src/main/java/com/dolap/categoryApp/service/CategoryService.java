package com.dolap.categoryApp.service;

import com.dolap.categoryApp.api.model.CategoryResponse;
import com.dolap.categoryApp.domain.entity.Category;
import com.dolap.categoryApp.domain.exception.RecordNotFoundException;
import com.dolap.categoryApp.domain.repository.CategoryRepository;
import com.dolap.categoryApp.mapper.CategoryMapper;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository repository;
    private final CategoryMapper categoryMapper;

    public List<CategoryResponse> getCategories() {
        final List<Category> rootCategories = repository.findAllByParentIdIsNull();
        return categoryMapper.convertToResponse(rootCategories);
    }

    public CategoryResponse getCategoryById(String categoryId) {
        final Category category = findCategoryById(categoryId);
        return categoryMapper.convertToResponse(category);
    }

    private Category findCategoryById(String categoryId) {
        return repository.findById(categoryId)
                .orElseThrow(() -> new RecordNotFoundException("Category not found."));
    }

    public List<CategoryResponse> getChildCategories(String categoryId) {
        final List<Category> children = repository.findAllByParentId(categoryId);
        return categoryMapper.convertToResponse(children);
    }
}
