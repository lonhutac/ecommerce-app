package com.dolap.categoryApp.configuration;

import com.dolap.categoryApp.domain.exception.ErrorResponse;
import com.dolap.categoryApp.domain.exception.RecordNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CategoryControllerAdvice {

    @ExceptionHandler(RecordNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse recordNotFoundHandler(RecordNotFoundException e) {
        return aErrorResponse(HttpStatus.NOT_FOUND.value(), e);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse exceptionHandler(Exception e) {
        return aErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
    }

    private ErrorResponse aErrorResponse(int status, Throwable exception) {
        return ErrorResponse.builder()
                .code(status)
                .message(exception.getMessage())
                .build();
    }
}
