package com.dolap.categoryApp.configuration.db.migration;

import com.dolap.categoryApp.domain.entity.Category;
import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ChangeLog(order = "001")
public class ChangeLogAddCategories {

    @ChangeSet(order = "001", id = "v001_createCategories", author = "mongoBeeBot")
    public void createCategories(MongoTemplate mongoTemplate) {

        log.info("Starting category import ...");

        // SHOES
        Category shoesCategory = buildCategory("Shoes");
        Category savedShoesCategory = mongoTemplate.save(shoesCategory);

        List<Category> childCategoriesOfShoes = buildChildCategories(Arrays.asList("Daily Shoes", "Basketball Shoes"),
                savedShoesCategory.getId());
        List<Category> childCategories = new ArrayList<>(childCategoriesOfShoes);

        // CLOTHING
        Category clothingCategory = buildCategory("Clothing");
        Category savedClothingCategory = mongoTemplate.save(clothingCategory);

        List<Category> childCategoriesOfClothing = buildChildCategories(Arrays.asList("Shirts", "Jeans", "Pants"),
                savedClothingCategory.getId());
        childCategories.addAll(childCategoriesOfClothing);

        childCategories.forEach(mongoTemplate::save);

        log.info("Successfully completed category import ...");
    }


    private Category buildCategory(String name) {
        return Category.builder()
                .name(name)
                .build();
    }

    private List<Category> buildChildCategories(List<String> names, String parentId) {
        return names.stream()
                .map(name -> Category.builder()
                        .name(name)
                        .parentId(parentId)
                        .build()
                ).collect(Collectors.toList());
    }
}
