package com.dolap.categoryApp.configuration;

import com.dolap.categoryApp.mapper.CategoryMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    @Bean
    public CategoryMapper categoryMapper() {
        return new CategoryMapper();
    }
}
