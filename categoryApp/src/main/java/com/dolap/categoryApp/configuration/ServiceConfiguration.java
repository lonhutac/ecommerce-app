package com.dolap.categoryApp.configuration;

import com.dolap.categoryApp.domain.repository.CategoryRepository;
import com.dolap.categoryApp.mapper.CategoryMapper;
import com.dolap.categoryApp.service.CategoryService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    @Bean
    public CategoryService categoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        return new CategoryService(categoryRepository, categoryMapper);
    }
}
