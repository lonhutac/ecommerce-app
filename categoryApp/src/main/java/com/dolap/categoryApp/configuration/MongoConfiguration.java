package com.dolap.categoryApp.configuration;


import com.dolap.categoryApp.configuration.db.migration.MongoChangeLogs;
import com.github.mongobee.Mongobee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableMongoAuditing
public class MongoConfiguration {

    @Value("${spring.data.mongodb.uri}")
    private String databaseUri;

    @Value("${spring.data.mongodb.database}")
    private String databaseName;

    @Bean
    public Mongobee mongobee(MongoTemplate mongoTemplate, Environment environment) {
        Mongobee runner = new Mongobee(databaseUri);
        runner.setDbName(databaseName);
        runner.setChangeLogsScanPackage(MongoChangeLogs.class.getPackage().getName());
        runner.setSpringEnvironment(environment);
        runner.setMongoTemplate(mongoTemplate);
        runner.setEnabled(true);
        return runner;
    }
}

