package com.dolap.categoryApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class CategoryAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(CategoryAppApplication.class, args);
    }

}
