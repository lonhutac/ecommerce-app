package com.dolap.categoryApp.domain.repository;

import com.dolap.categoryApp.domain.entity.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CategoryRepository extends MongoRepository<Category, String> {

    List<Category> findAllByParentIdIsNull();

    List<Category> findAllByParentId(String parentId);
}
