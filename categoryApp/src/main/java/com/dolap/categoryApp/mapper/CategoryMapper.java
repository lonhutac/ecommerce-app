package com.dolap.categoryApp.mapper;

import com.dolap.categoryApp.api.model.CategoryResponse;
import com.dolap.categoryApp.domain.entity.Category;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryMapper {

    public List<CategoryResponse> convertToResponse(List<Category> categories) {
        return categories.stream()
                .map(this::convertToResponse)
                .collect(Collectors.toList());
    }

    public CategoryResponse convertToResponse(Category category) {
        return CategoryResponse.builder()
                .id(category.getId())
                .name(category.getName())
                .parentId(category.getParentId())
                .build();
    }
}
