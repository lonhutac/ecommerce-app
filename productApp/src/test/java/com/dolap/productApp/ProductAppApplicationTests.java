package com.dolap.productApp;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "spring.data.mongodb.uri:mongodb://localhost:27018/productDB",
        "spring.data.mongodb.database:productDB",
        "platform.db.mongobee.enabled:true"
})
public class ProductAppApplicationTests {

    private static final String IP_MONGODB = "localhost";
    private static final int PORT_MONGODB = 27018;
    private static MongodExecutable mongodExecutable;

    @BeforeClass
    public static void setUpClass() throws IOException {
        startEmbeddedMongoDb();
    }

    @Test
    public void contextLoads() {
    }

    @AfterClass
    public static void cleanClass() {
        stopEmbeddedMongoDb();
    }

    private static void startEmbeddedMongoDb() throws IOException {
        log.info("Embedded MongoDB is starting...");
        IMongodConfig mongodConfig = new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(IP_MONGODB, PORT_MONGODB, Network.localhostIsIPv6()))
                .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
        mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();
        log.info("Embedded MongoDB started.");
    }

    private static void stopEmbeddedMongoDb() {
        log.info("Embedded MongoDB is stoping...");
        mongodExecutable.stop();
        log.info("Embedded MongoDB stopped.");
    }

}
