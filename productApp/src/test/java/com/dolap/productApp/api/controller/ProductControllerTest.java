package com.dolap.productApp.api.controller;

import com.dolap.productApp.api.model.request.ProductCreateRequest;
import com.dolap.productApp.api.model.request.ProductUpdateRequest;
import com.dolap.productApp.api.model.response.CreateResponse;
import com.dolap.productApp.api.model.response.ProductResponse;
import com.dolap.productApp.domain.ColorCodeEnum;
import com.dolap.productApp.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProductService productService;

    /**
     * {@link ProductController#createProduct(ProductCreateRequest)}
     */
    @Test
    @SneakyThrows
    public void should_create_product() {

        // given
        final ProductCreateRequest request = objectMapper.readValue(new ClassPathResource("data/CREATE_PRODUCT.json").getFile(), ProductCreateRequest.class);
        final CreateResponse response = CreateResponse.builder()
                .id("productId")
                .build();

        // when
        when(productService.createProduct(ArgumentMatchers.any(ProductCreateRequest.class))).thenReturn(response);
        final ResultActions resultActions = mockMvc
                .perform(post("/api/v1/products")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        resultActions.andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists());
        verify(productService, times(1)).createProduct(ArgumentMatchers.any(ProductCreateRequest.class));
    }

    /**
     * {@link ProductController#updateProduct(String, ProductUpdateRequest)}
     */
    @Test
    @SneakyThrows
    public void should_update_product() {

        // given
        final ProductUpdateRequest request = objectMapper.readValue(new ClassPathResource("data/UPDATE_PRODUCT.json").getFile(), ProductUpdateRequest.class);

        // when
        doNothing().when(productService).updateProduct(anyString(), ArgumentMatchers.any(ProductUpdateRequest.class));
        final ResultActions resultActions = mockMvc
                .perform(put("/api/v1/products/testId")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        resultActions.andExpect(status().isAccepted());
        verify(productService, times(1)).updateProduct(anyString(), ArgumentMatchers.any(ProductUpdateRequest.class));
    }

    /**
     * {@link ProductController#getProductsByCategory(String)}
     */
    @Test
    @SneakyThrows
    public void should_list_product_by_category() {

        // given
        final List<ProductResponse> expectedProducts = buildProducts();

        // when
        when(productService.getProductsByCategory(anyString())).thenReturn(expectedProducts);
        final ResultActions resultActions = mockMvc
                .perform(get("/api/v1/products?categoryId=categoryId1"))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id").exists())
                .andExpect(jsonPath("$.[*].name").exists())
                .andExpect(jsonPath("$.[*].description").exists())
                .andExpect(jsonPath("$.[*].categoryId").exists())
                .andExpect(jsonPath("$.[*].colorCode").exists())
                .andExpect(jsonPath("$.[*].size").exists())
                .andExpect(jsonPath("$.[*].price").exists());
    }

    private List<ProductResponse> buildProducts() {
        return Arrays.asList(
                ProductResponse.builder()
                        .id("productId1")
                        .name("productName1")
                        .description("description")
                        .categoryId("categoryId")
                        .colorCode(ColorCodeEnum.BLACK)
                        .images(Collections.singletonList("image1"))
                        .size("M")
                        .price(new BigDecimal(50))
                        .build(),
                ProductResponse.builder()
                        .id("productId2")
                        .name("productName2")
                        .description("description")
                        .categoryId("categoryId")
                        .colorCode(ColorCodeEnum.BLACK)
                        .images(Collections.singletonList("image2"))
                        .size("M")
                        .price(new BigDecimal(80))
                        .build()
        );
    }
}
