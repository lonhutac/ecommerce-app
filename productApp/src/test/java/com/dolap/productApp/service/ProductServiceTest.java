package com.dolap.productApp.service;

import com.dolap.productApp.api.model.request.ProductCreateRequest;
import com.dolap.productApp.api.model.request.ProductUpdateRequest;
import com.dolap.productApp.api.model.response.CategoryResponse;
import com.dolap.productApp.api.model.response.CreateResponse;
import com.dolap.productApp.api.model.response.ProductResponse;
import com.dolap.productApp.client.CategoryApiClient;
import com.dolap.productApp.domain.ColorCodeEnum;
import com.dolap.productApp.domain.entity.Product;
import com.dolap.productApp.domain.exception.RecordNotFoundException;
import com.dolap.productApp.domain.mapper.ProductMapper;
import com.dolap.productApp.domain.repository.ProductRepository;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {


    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    @Mock
    private CategoryApiClient categoryApiClient;

    @Before
    public void setUp() {
        productService = new ProductService(productRepository, productMapper, categoryApiClient);
    }

    /**
     * {@link ProductService#createProduct(ProductCreateRequest)}
     */
    @Test
    public void should_create_product() {

        //given
        final ProductCreateRequest request = productCreateRequest();
        final Product productWithoutId = productWithoutId();
        final Product product = product();
        final CreateResponse expectedResponse = CreateResponse.builder().id(product.getId()).build();

        // when
        when(productMapper.convertToEntity(request)).thenReturn(productWithoutId);
        when(productRepository.save(productWithoutId)).thenReturn(product);
        when(productMapper.convertToCreateResponse(product)).thenReturn(expectedResponse);

        //then
        final CreateResponse response = productService.createProduct(request);

        assertThat(response.getId(), equalTo(expectedResponse.getId()));
    }

    /**
     * {@link ProductService#getProductsByCategory(String)}
     */
    @Test
    public void should_list_product_by_category() {

        // given
        final String parentCategoryId = "parentCategoryId1";
        final String childCategoryId = "categoryId1";
        final List<CategoryResponse> childCategories = childCategories(childCategoryId);
        final List<Product> products = products();
        final List<ProductResponse> expectedResponse = productResponses();

        // when
        when(categoryApiClient.getChildrenOfCategory(parentCategoryId)).thenReturn(childCategories);
        when(productRepository.findAllByCategoryIdIn(anySet())).thenReturn(products);
        when(productMapper.convertToResponse(anyList())).thenReturn(expectedResponse);

        // then
        final List<ProductResponse> response = productService.getProductsByCategory(parentCategoryId);
        assertThat(response.size(), equalTo(expectedResponse.size()));
        assertThat(expectedResponse.get(0).getId(), equalTo(response.get(0).getId()));
        assertThat(expectedResponse.get(0).getName(), equalTo(response.get(0).getName()));
        assertThat(expectedResponse.get(0).getCategoryId(), equalTo(response.get(0).getCategoryId()));
        assertThat(expectedResponse.get(1).getId(), equalTo(response.get(1).getId()));
        assertThat(expectedResponse.get(1).getName(), equalTo(response.get(1).getName()));
        assertThat(expectedResponse.get(1).getCategoryId(), equalTo(response.get(1).getCategoryId()));

    }

    /**
     * {@link ProductService#updateProduct(String, ProductUpdateRequest)}
     */
    @Test
    @SneakyThrows
    public void should_update_product() {

        // given
        final ProductUpdateRequest request = productUpdateRequest();

        // when
        when(productRepository.findById(anyString())).thenReturn(Optional.of(new Product()));
        doNothing().when(productMapper).updateRequestToEntity(ArgumentMatchers.any(Product.class),
                ArgumentMatchers.any(ProductUpdateRequest.class));
        when(productRepository.save(ArgumentMatchers.any(Product.class))).thenReturn(new Product());

        // then
        productService.updateProduct("productId", request);

        verify(productMapper, times(1))
                .updateRequestToEntity(ArgumentMatchers.any(Product.class), ArgumentMatchers.any(ProductUpdateRequest.class));
        verify(productRepository, times(1))
                .save(ArgumentMatchers.any(Product.class));
        verify(productRepository, times(1))
                .findById(anyString());
    }

    /**
     * {@link ProductService#updateProduct(String, ProductUpdateRequest)}
     */
    @Test(expected = RecordNotFoundException.class)
    @SneakyThrows
    public void should_get_exception_when_updating_product_with_invalid_id() {

        // given
        final ProductUpdateRequest request = productUpdateRequest();

        // when
        when(productRepository.findById(anyString())).thenThrow(RecordNotFoundException.class);

        // then
        productService.updateProduct("productId", request);

        fail("Should throw RecordNotFoundException when updating product with invalid product id");
    }

    private List<CategoryResponse> childCategories(String childCategoryId) {
        return Collections.singletonList(
                CategoryResponse.builder()
                        .id(childCategoryId)
                        .name("categoryName1")
                        .parentId("parentCategoryId1")
                        .build()
        );
    }

    private ProductCreateRequest productCreateRequest() {
        return ProductCreateRequest.builder()
                .name("productName2")
                .description("description")
                .categoryId("categoryId")
                .colorCode(ColorCodeEnum.BLACK)
                .images(Collections.singletonList("image2"))
                .size("M")
                .price(new BigDecimal(80))
                .build();
    }

    private ProductUpdateRequest productUpdateRequest() {
        return ProductUpdateRequest.builder()
                .name("productName1")
                .description("description")
                .categoryId("categoryId")
                .colorCode(ColorCodeEnum.BLACK)
                .images(Collections.singletonList("image2"))
                .size("M")
                .price(new BigDecimal(80))
                .build();
    }

    private Product productWithoutId() {
        return Product.builder()
                .name("productName1")
                .description("description")
                .categoryId("categoryId")
                .colorCode(ColorCodeEnum.BLACK)
                .images(Collections.singletonList("image1"))
                .size("M")
                .price(new BigDecimal(80))
                .build();
    }

    private Product product() {
        return Product.builder()
                .id("productId1")
                .name("productName1")
                .description("description")
                .categoryId("categoryId")
                .colorCode(ColorCodeEnum.BLACK)
                .images(Collections.singletonList("image1"))
                .size("M")
                .price(new BigDecimal(80))
                .build();
    }

    private List<Product> products() {
        return Arrays.asList(
                Product.builder()
                        .id("productId1")
                        .name("productName1")
                        .description("description")
                        .categoryId("parentCategoryId")
                        .colorCode(ColorCodeEnum.BLACK)
                        .images(Collections.singletonList("image1"))
                        .size("M")
                        .price(new BigDecimal(80))
                        .build(),
                Product.builder()
                        .id("productId1")
                        .name("productName1")
                        .description("description")
                        .categoryId("categoryId1")
                        .colorCode(ColorCodeEnum.BLACK)
                        .images(Collections.singletonList("image1"))
                        .size("M")
                        .price(new BigDecimal(80))
                        .build()
        );
    }

    private List<ProductResponse> productResponses() {
        return Arrays.asList(
                ProductResponse.builder()
                        .id("productId1")
                        .name("productName1")
                        .description("description")
                        .categoryId("parentCategoryId")
                        .colorCode(ColorCodeEnum.BLACK)
                        .images(Collections.singletonList("image1"))
                        .size("M")
                        .price(new BigDecimal(80))
                        .build(),
                ProductResponse.builder()
                        .id("productId1")
                        .name("productName1")
                        .description("description")
                        .categoryId("categoryId1")
                        .colorCode(ColorCodeEnum.BLACK)
                        .images(Collections.singletonList("image1"))
                        .size("M")
                        .price(new BigDecimal(80))
                        .build()
        );
    }
}
