package com.dolap.productApp.client;

import com.dolap.productApp.api.model.response.CategoryResponse;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface CategoryApiClient {

    @RequestLine("GET /api/v1/categories/{categoryId}/children")
    List<CategoryResponse> getChildrenOfCategory(@Param("categoryId") String categoryId);

    @RequestLine("GET /api/v1/categories/{categoryId}")
    CategoryResponse getCategory(String categoryId);
}
