package com.dolap.productApp.api.controller;

import com.dolap.productApp.api.model.request.ProductCreateRequest;
import com.dolap.productApp.api.model.request.ProductUpdateRequest;
import com.dolap.productApp.api.model.response.CreateResponse;
import com.dolap.productApp.api.model.response.ProductResponse;
import com.dolap.productApp.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateResponse createProduct(@RequestBody @Valid ProductCreateRequest request) {
        return productService.createProduct(request);
    }

    @PutMapping("/{productId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateProduct(@PathVariable String productId, @RequestBody ProductUpdateRequest request) {
        productService.updateProduct(productId, request);
    }

    @GetMapping
    public List<ProductResponse> getProductsByCategory(@RequestParam String categoryId) {
        return productService.getProductsByCategory(categoryId);
    }
}
