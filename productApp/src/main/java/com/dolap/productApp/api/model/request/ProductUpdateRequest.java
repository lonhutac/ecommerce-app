package com.dolap.productApp.api.model.request;

import com.dolap.productApp.domain.ColorCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductUpdateRequest {

    private String name;
    private String description;
    private List<String> images;
    private String categoryId;
    private ColorCodeEnum colorCode;
    private String size;
    private BigDecimal price;
}
