package com.dolap.productApp.api.model.response;

import com.dolap.productApp.domain.ColorCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {

    private String id;
    private String name;
    private String description;
    private List<String> images;
    private String categoryId;
    private ColorCodeEnum colorCode;
    private String size;
    private BigDecimal price;
}
