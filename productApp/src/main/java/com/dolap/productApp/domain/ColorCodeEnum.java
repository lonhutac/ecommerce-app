package com.dolap.productApp.domain;

public enum ColorCodeEnum {

    BLUE,
    GREEN,
    GRAY,
    BLACK,
    PINK
}
