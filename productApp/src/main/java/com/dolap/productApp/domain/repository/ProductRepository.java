package com.dolap.productApp.domain.repository;

import com.dolap.productApp.domain.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Set;

public interface ProductRepository extends MongoRepository<Product, String> {

    List<Product> findAllByCategoryIdIn(Set<String> categoryIds);
}
