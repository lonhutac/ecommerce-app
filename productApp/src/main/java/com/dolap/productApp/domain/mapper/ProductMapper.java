package com.dolap.productApp.domain.mapper;

import com.dolap.productApp.api.model.request.ProductCreateRequest;
import com.dolap.productApp.api.model.request.ProductUpdateRequest;
import com.dolap.productApp.api.model.response.CreateResponse;
import com.dolap.productApp.api.model.response.ProductResponse;
import com.dolap.productApp.domain.entity.Product;

import java.util.List;
import java.util.stream.Collectors;

public class ProductMapper {

    public List<ProductResponse> convertToResponse(List<Product> products) {
        return products.stream()
                .map(this::convertToResponse)
                .collect(Collectors.toList());
    }

    private ProductResponse convertToResponse(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .images(product.getImages())
                .categoryId(product.getCategoryId())
                .price(product.getPrice())
                .size(product.getSize())
                .colorCode(product.getColorCode())
                .build();
    }

    public Product convertToEntity(ProductCreateRequest request) {
        return Product.builder()
                .name(request.getName())
                .description(request.getDescription())
                .images(request.getImages())
                .categoryId(request.getCategoryId())
                .colorCode(request.getColorCode())
                .size(request.getSize())
                .price(request.getPrice())
                .build();
    }

    public CreateResponse convertToCreateResponse(Product product) {
        return CreateResponse.builder()
                .id(product.getId())
                .build();
    }

    public void updateRequestToEntity(Product product, ProductUpdateRequest request) {
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setCategoryId(request.getCategoryId());
        product.setColorCode(request.getColorCode());
        product.setImages(request.getImages());
        product.setSize(request.getSize());
        product.setPrice(request.getPrice());
    }
}
