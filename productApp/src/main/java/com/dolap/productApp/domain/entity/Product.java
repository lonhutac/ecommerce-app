package com.dolap.productApp.domain.entity;

import com.dolap.productApp.domain.ColorCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "products")
public class Product {

    @Id
    private String id;

    @Field
    private String name;

    @Field
    private String description;

    @Field
    private List<String> images;

    @Field
    private String categoryId;

    @Field
    private ColorCodeEnum colorCode;

    @Field
    private String size;

    @Field
    private BigDecimal price;
}
