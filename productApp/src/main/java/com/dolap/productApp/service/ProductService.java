package com.dolap.productApp.service;

import com.dolap.productApp.api.model.request.ProductCreateRequest;
import com.dolap.productApp.api.model.request.ProductUpdateRequest;
import com.dolap.productApp.api.model.response.CategoryResponse;
import com.dolap.productApp.api.model.response.CreateResponse;
import com.dolap.productApp.api.model.response.ProductResponse;
import com.dolap.productApp.client.CategoryApiClient;
import com.dolap.productApp.domain.entity.Product;
import com.dolap.productApp.domain.exception.RecordNotFoundException;
import com.dolap.productApp.domain.mapper.ProductMapper;
import com.dolap.productApp.domain.repository.ProductRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final CategoryApiClient categoryApiClient;

    public CreateResponse createProduct(ProductCreateRequest request) {
        final Product product = productMapper.convertToEntity(request);
        final Product savedProduct = productRepository.save(product);
        return productMapper.convertToCreateResponse(savedProduct);
    }

    public void updateProduct(String productId, ProductUpdateRequest request) {
        final Product product = findProduct(productId);
        productMapper.updateRequestToEntity(product, request);
        productRepository.save(product);
    }

    public List<ProductResponse> getProductsByCategory(String categoryId) {
        final List<CategoryResponse> childrenOfCategory = categoryApiClient.getChildrenOfCategory(categoryId);
        final Set<String> requestedCategoryIds = childrenOfCategory.stream()
                .map(CategoryResponse::getId)
                .collect(Collectors.toSet());
        requestedCategoryIds.add(categoryId);
        final List<Product> products = productRepository.findAllByCategoryIdIn(requestedCategoryIds);
        return productMapper.convertToResponse(products);
    }

    private Product findProduct(String productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new RecordNotFoundException("Product not found"));
    }
}
