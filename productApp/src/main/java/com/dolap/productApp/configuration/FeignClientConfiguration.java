package com.dolap.productApp.configuration;

import com.dolap.productApp.client.CategoryApiClient;
import feign.Client;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableFeignClients
@Import(FeignClientsConfiguration.class)
public class FeignClientConfiguration {

    @Bean
    public CategoryApiClient platformAddonsClient(Decoder feignDecoder, Encoder feignEncoder, Client client) {
        return Feign.builder()
                .client(client)
                .encoder(feignEncoder)
                .decoder(feignDecoder)
                .target(CategoryApiClient.class, "http://category-api");
    }
}
