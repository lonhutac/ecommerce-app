package com.dolap.productApp.configuration;

import com.dolap.productApp.domain.exception.ErrorResponse;
import com.dolap.productApp.domain.exception.RecordNotFoundException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ProductControllerAdvice {

    @ExceptionHandler(RecordNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse recordNotFoundHandler(RecordNotFoundException e) {
        return aErrorResponse(HttpStatus.NOT_FOUND.value(), e);
    }

    @ExceptionHandler(MismatchedInputException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mismatchedInputExceptionHandler(MismatchedInputException e) {
        return aErrorResponse(HttpStatus.BAD_REQUEST.value(), e);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse exceptionHandler(MethodArgumentNotValidException e) {
        return aErrorResponse(HttpStatus.BAD_REQUEST.value(), e);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse exceptionHandler(Exception e) {
        return aErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
    }

    private ErrorResponse aErrorResponse(int status, Throwable exception) {
        return ErrorResponse.builder()
                .code(status)
                .message(exception.getMessage())
                .build();
    }
}
