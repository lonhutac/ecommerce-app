package com.dolap.productApp.configuration;

import com.dolap.productApp.domain.mapper.ProductMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    @Bean
    public ProductMapper productMapper() {
        return new ProductMapper();
    }
}
