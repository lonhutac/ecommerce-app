package com.dolap.productApp.configuration;

import com.dolap.productApp.client.CategoryApiClient;
import com.dolap.productApp.domain.mapper.ProductMapper;
import com.dolap.productApp.domain.repository.ProductRepository;
import com.dolap.productApp.service.ProductService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    @Bean
    public ProductService productService(ProductRepository productRepository,
                                         ProductMapper productMapper,
                                         CategoryApiClient categoryApiClient) {
        return new ProductService(productRepository, productMapper, categoryApiClient);
    }
}
